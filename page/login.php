<?php
$login = "acesso@dgssistemas.net";
$senha = "123";

    if(isset($_POST["login"])){
         if($_POST["login"] == $login and $_POST["senha"] == $senha){
          
          $login = $_POST['login'];
          $senha = $_POST['senha'];

          session_start();
          $_SESSION['emailSession']= $login;
          $_SESSION['senhaSession']= $senha;
          
     
          header("Location:sidebar/dashboard.php"); 
        }else{
          echo "<script>alert('Usuário e senha não correspondem.'); history.back();</script>";
        }
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Douglas Rafael Esquinelato">
	  <meta name="description" content="Sistema de cotação do açúcar em tempo real">
	  <meta name="keywords" content="açúcar, cotações , website, agronegocio, agricola, agronomia, fatec, pompeia, big data">  
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="57x57" href="icone/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="icone/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="icone/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="icone/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="icone/apple-icon-114x114.png">  
    <link rel="apple-touch-icon" sizes="120x120" href="icone/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="icone/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="icone/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="icone/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="icone/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="icone/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="icone/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="icone/favicon-16x16.png">
    <link rel="manifest" href="icone/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icone/ms-icon-144x144.png">
    <meta name="theme-color" content="#ff6a36">
    <link rel="stylesheet" href="style/style.css">
    <title>AgroCota</title> 
  </head>

  <body>
    <div class="fundo">
      <center>
      <div class="login">
        <div class="login-img"> </div>
        <form method="post">
          <div class="form-group">
            <label>E-mail</label>
            <input type="email" class="form-control" name="login" required>
          </div>
          <div class="form-group">
            <label>Senha</label>
            <input type="password" class="form-control" name="senha" required>
          </div>
            <button type="submit" class="btn btn-primary btn-sm btn-block">Acessar</button>
        </form>
      </div>
      </center>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  </body>
</html>