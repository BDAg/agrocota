<?php
require "../config/conexao.php";
$link = Conectar();
$sqll = mysqli_query($link, "select * from `webdados` ORDER BY idCod DESC LIMIT 1");
?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Douglas Rafael Esquinelato">
  <meta name="description" content="Sistema de cotação do açúcar em tempo real">
  <meta name="keywords" content="açúcar, cotações , website, agronegocio, agricola, agronomia, fatec, pompeia, big data">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Agrocota</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="apple-touch-icon" sizes="57x57" href="../icone/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="../icone/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="../icone/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="../icone/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="../icone/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="../icone/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="../icone/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="../icone/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="../icone/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="../icone/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../icone/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="../icone/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../icone/favicon-16x16.png">
  <link rel="manifest" href="../icone/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="../icone/ms-icon-144x144.png">
  <meta name="theme-color" content="#ff6a36">
  <!-- Bootstrap CSS CDN -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <!-- Our Custom CSS -->
  <link rel="stylesheet" href="../style/styleSider.css">
  <link rel="stylesheet" href="../style/style.css">
</head>

<body>

  <!-- Font Awesome JS -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"></script>
  <div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
      <div class="sidebar-header">
        <img src="../img/Agrocota.png" width="200">
      </div>
      <ul class="list-unstyled">
        <li>
          <a href="dashboard.php">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-award" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M9.669.864L8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193l-1.51-.229L8 1.126l-1.355.702-1.51.229-.684 1.365-1.086 1.072L3.614 6l-.25 1.506 1.087 1.072.684 1.365 1.51.229L8 10.874l1.356-.702 1.509-.229.684-1.365 1.086-1.072L12.387 6l.248-1.506-1.086-1.072-.684-1.365z" />
              <path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z" />
            </svg>
            Cotação do açúcar
          </a>
        </li>
        <li>
          <a href="noticias.php">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-rss" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
              <path d="M5.5 12a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
              <path fill-rule="evenodd" d="M2.5 3.5a1 1 0 0 1 1-1c5.523 0 10 4.477 10 10a1 1 0 1 1-2 0 8 8 0 0 0-8-8 1 1 0 0 1-1-1zm0 4a1 1 0 0 1 1-1 6 6 0 0 1 6 6 1 1 0 1 1-2 0 4 4 0 0 0-4-4 1 1 0 0 1-1-1z" />
            </svg>
            RSS de Notícias
          </a>
        </li>
        <li>
          <a href="clima.php">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="1em" height="1em" viewBox="0 0 172 172" style=" fill:#000000;">
              <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                <path d="M0,172v-172h172v172z" fill="none"></path>
                <g fill="#ffffff">
                  <path d="M58.1575,6.7725c-0.14781,0.02688 -0.29562,0.06719 -0.43,0.1075c-1.59906,0.36281 -2.72781,1.80063 -2.6875,3.44v17.2c-0.01344,1.23625 0.63156,2.39188 1.70656,3.02344c1.075,0.61813 2.39187,0.61813 3.46687,0c1.075,-0.63156 1.72,-1.78719 1.70656,-3.02344v-17.2c0.04031,-0.99437 -0.36281,-1.94844 -1.075,-2.62031c-0.72562,-0.68531 -1.70656,-1.02125 -2.6875,-0.92719zM19.2425,22.8975c-0.14781,0.02688 -0.29562,0.06719 -0.43,0.1075c-1.29,0.22844 -2.32469,1.16906 -2.6875,2.41875c-0.36281,1.26313 0.01344,2.60688 0.9675,3.49375l12.255,12.1475c1.37063,1.33031 3.56094,1.31688 4.89125,-0.05375c1.33031,-1.37062 1.31688,-3.56094 -0.05375,-4.89125l-12.1475,-12.1475c-0.71219,-0.76594 -1.74687,-1.15562 -2.795,-1.075zM97.0725,22.8975c-0.14781,0.02688 -0.29562,0.06719 -0.43,0.1075c-0.65844,0.14781 -1.24969,0.48375 -1.72,0.9675l-12.1475,12.1475c-1.37062,1.37063 -1.37062,3.57438 0,4.945c1.37063,1.37063 3.57438,1.37063 4.945,0l12.1475,-12.1475c1.1825,-0.99437 1.55875,-2.67406 0.90031,-4.07156c-0.65844,-1.41094 -2.16344,-2.20375 -3.69531,-1.94844zM58.1575,37.84c-0.1075,0.02688 -0.215,0.06719 -0.3225,0.1075c-0.215,0.01344 -0.43,0.05375 -0.645,0.1075c-0.04031,0.04031 -0.06719,0.06719 -0.1075,0.1075c-14.45875,0.77938 -26.1225,12.56406 -26.1225,27.1975c0,10.19906 5.56313,19.12156 13.8675,23.865c1.06156,0.77938 2.4725,0.88688 3.64156,0.28219c1.16906,-0.61812 1.88125,-1.84094 1.84094,-3.15781c-0.04031,-1.33031 -0.83312,-2.49938 -2.0425,-3.03688c-6.22156,-3.56094 -10.4275,-10.26625 -10.4275,-17.9525c0,-11.32781 9.12406,-20.51906 20.425,-20.64c0.06719,0 0.14781,0 0.215,0c0.1075,0 0.215,0 0.3225,0c4.98531,0.08063 9.47344,1.935 13.0075,4.945c1.45125,1.24969 3.64156,1.075 4.89125,-0.37625c1.24969,-1.45125 1.075,-3.64156 -0.37625,-4.89125c-4.4075,-3.7625 -10.105,-5.85875 -16.2325,-6.235c-0.1075,-0.04031 -0.215,-0.08062 -0.3225,-0.1075c-0.34937,-0.13437 -0.71219,-0.20156 -1.075,-0.215c-0.06719,0 -0.14781,0 -0.215,0c-0.1075,0 -0.215,0 -0.3225,0zM99.76,48.16c-18.96031,0 -34.4,15.43969 -34.4,34.4c0,1.3975 0.25531,2.72781 0.43,4.085c-11.77125,1.55875 -21.07,11.24719 -21.07,23.435c0,13.26281 10.81719,24.08 24.08,24.08h72.24c17.05219,0 30.96,-13.90781 30.96,-30.96c0,-17.05219 -13.90781,-30.96 -30.96,-30.96c-3.01,0 -5.73781,0.91375 -8.4925,1.72c-3.88344,-14.74094 -16.85062,-25.8 -32.7875,-25.8zM99.76,55.04c13.81375,0 25.16844,10.11844 27.1975,23.3275l0.645,4.4075l4.085,-1.72c2.87563,-1.22281 6.02,-1.935 9.3525,-1.935c13.34344,0 24.08,10.73656 24.08,24.08c0,13.34344 -10.73656,24.08 -24.08,24.08h-72.24c-9.54062,0 -17.2,-7.65937 -17.2,-17.2c0,-9.54062 7.65938,-17.2 17.2,-17.2c0.01344,0 0.20156,-0.01344 0.645,0l4.515,0.215l-0.9675,-4.4075c-0.45687,-2.00219 -0.7525,-4.03125 -0.7525,-6.1275c0,-15.23812 12.28188,-27.52 27.52,-27.52zM2.4725,61.92c-1.89469,0.26875 -3.225,2.02906 -2.95625,3.92375c0.26875,1.89469 2.02906,3.225 3.92375,2.95625h17.2c1.23625,0.01344 2.39188,-0.63156 3.02344,-1.70656c0.61813,-1.075 0.61813,-2.39187 0,-3.46687c-0.63156,-1.075 -1.78719,-1.72 -3.02344,-1.70656h-17.2c-0.1075,0 -0.215,0 -0.3225,0c-0.1075,0 -0.215,0 -0.3225,0c-0.1075,0 -0.215,0 -0.3225,0zM31.39,88.58c-0.14781,0.02688 -0.29562,0.06719 -0.43,0.1075c-0.61812,0.16125 -1.16906,0.49719 -1.6125,0.9675l-12.255,12.1475c-1.37062,1.37063 -1.37062,3.57438 0,4.945c1.37063,1.37063 3.57438,1.37063 4.945,0l12.1475,-12.255c1.075,-1.02125 1.38406,-2.62031 0.7525,-3.96406c-0.645,-1.35719 -2.06937,-2.13656 -3.5475,-1.94844zM75.25,141.04c-0.76594,0.08063 -1.49156,0.43 -2.0425,0.9675l-6.88,6.88c-1.37062,1.37063 -1.37062,3.57438 0,4.945c1.37063,1.37063 3.57438,1.37063 4.945,0l6.88,-6.88c1.11531,-1.03469 1.41094,-2.67406 0.73906,-4.03125c-0.65844,-1.37062 -2.15,-2.12312 -3.64156,-1.88125zM140.61,141.04c-0.76594,0.08063 -1.49156,0.43 -2.0425,0.9675l-6.88,6.88c-1.37062,1.37063 -1.37062,3.57438 0,4.945c1.37063,1.37063 3.57438,1.37063 4.945,0l6.88,-6.88c1.11531,-1.03469 1.41094,-2.67406 0.73906,-4.03125c-0.65844,-1.37062 -2.15,-2.12312 -3.64156,-1.88125zM106.21,154.8c-0.76594,0.08063 -1.49156,0.43 -2.0425,0.9675l-6.88,6.88c-1.37062,1.37063 -1.37062,3.57438 0,4.945c1.37063,1.37063 3.57438,1.37063 4.945,0l6.88,-6.88c1.11531,-1.03469 1.41094,-2.67406 0.73906,-4.03125c-0.65844,-1.37062 -2.15,-2.12312 -3.64156,-1.88125z"></path>
                </g>
              </g>
            </svg>
            Clima / Tempo
          </a>
        </li>
        <li>
          <a href="sobre.php">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-hand-thumbs-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M6.956 1.745C7.021.81 7.908.087 8.864.325l.261.066c.463.116.874.456 1.012.965.22.816.533 2.511.062 4.51a9.84 9.84 0 0 1 .443-.051c.713-.065 1.669-.072 2.516.21.518.173.994.681 1.2 1.273.184.532.16 1.162-.234 1.733.058.119.103.242.138.363.077.27.113.567.113.856 0 .289-.036.586-.113.856-.039.135-.09.273-.16.404.169.387.107.819-.003 1.148a3.163 3.163 0 0 1-.488.901c.054.152.076.312.076.465 0 .305-.089.625-.253.912C13.1 15.522 12.437 16 11.5 16v-1c.563 0 .901-.272 1.066-.56a.865.865 0 0 0 .121-.416c0-.12-.035-.165-.04-.17l-.354-.354.353-.354c.202-.201.407-.511.505-.804.104-.312.043-.441-.005-.488l-.353-.354.353-.354c.043-.042.105-.14.154-.315.048-.167.075-.37.075-.581 0-.211-.027-.414-.075-.581-.05-.174-.111-.273-.154-.315L12.793 9l.353-.354c.353-.352.373-.713.267-1.02-.122-.35-.396-.593-.571-.652-.653-.217-1.447-.224-2.11-.164a8.907 8.907 0 0 0-1.094.171l-.014.003-.003.001a.5.5 0 0 1-.595-.643 8.34 8.34 0 0 0 .145-4.726c-.03-.111-.128-.215-.288-.255l-.262-.065c-.306-.077-.642.156-.667.518-.075 1.082-.239 2.15-.482 2.85-.174.502-.603 1.268-1.238 1.977-.637.712-1.519 1.41-2.614 1.708-.394.108-.62.396-.62.65v4.002c0 .26.22.515.553.55 1.293.137 1.936.53 2.491.868l.04.025c.27.164.495.296.776.393.277.095.63.163 1.14.163h3.5v1H8c-.605 0-1.07-.081-1.466-.218a4.82 4.82 0 0 1-.97-.484l-.048-.03c-.504-.307-.999-.609-2.068-.722C2.682 14.464 2 13.846 2 13V9c0-.85.685-1.432 1.357-1.615.849-.232 1.574-.787 2.132-1.41.56-.627.914-1.28 1.039-1.639.199-.575.356-1.539.428-2.59z" />
            </svg>
            Sobre este projeto
          </a>
        </li>
        <li>
          <a href="cadastro.php">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-people" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1h7.956a.274.274 0 0 0 .014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C13.688 10.629 12.718 10 11 10c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 0 0 .022.004zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10c-1.668.02-2.615.64-3.16 1.276C1.163 11.97 1 12.739 1 13h3c0-1.045.323-2.086.92-3zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
            </svg>
            Suporte
          </a>
        </li>
        <li>
          <a href="../config/sair.php">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-door-closed" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M3 2a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v13h1.5a.5.5 0 0 1 0 1h-13a.5.5 0 0 1 0-1H3V2zm1 13h8V2H4v13z" />
              <path d="M9 9a1 1 0 1 0 2 0 1 1 0 0 0-2 0z" />
            </svg>
            Sair
          </a>
        </li>
      </ul>
    </nav>

    <!-- Page Content  -->
    <div id="content">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
          <button type="button" id="sidebarCollapse" class="btn btn-danger">
            <i class="fas fa-align-left"></i>
            <span>Fechar sidebar</span>
          </button>
          <tbody>
            <?php while ($row = mysqli_fetch_assoc($sqll)) { ?>
              <div class="cores">
                <tr>
                  <label class="principal">Vencimento</label>
                  <td><?php echo $row['vencimento'] ?></td>
                  <label class="principal">R$/Saca de 50 Kg</label>
                  <td><?php echo $row['saca'] ?></td>
                  <label class="principal">Variação Diária %</label>
                  <td><?php echo $row['variacao'] ?></td>
                </tr>
              </div>
            <?php  } ?>
          </tbody>
        </div>
      </nav>
      <div class="alert alert-primary" role="alert">
        <h6>Entre em contato com a gente!!!</h6>
      </div>

      <!-- Inicio do formulário -->
      <form method="post" action="../config/email.php">
        <div class="form-row">
          <div class="col-12">
            <label>Nome</label>
            <input type="text" class="form-control" name="nome" placeholder="Digite seu nome..." required>
          </div>
          <div class="col">
            <label>E-mail</label>
            <input type="text" class="form-control" name="email" placeholder="Digite seu e-mail..." required>
          </div>
          <div class="col">
            <label>Telefone</label>
            <input type="text" class="form-control" name="telefone" placeholder="Digite seu Telefone..." required>
          </div>
          <div class="col-12">
            <label>Assunto</label>
            <input type="text" class="form-control" name="assunto" placeholder="Digite o assunto..." required>
          </div>
        </div>
        <div class="form-group">
          <label>Mensagem</label>
          <textarea class="form-control" name="mensagem" id="mensagem-sucesso" placeholder="Digite sua mensagem..." rows="3" required></textarea>
        </div>
        <button type="submit" class="btn btn-primary btn-sm btn-block">Enviar</button>
      </form>
      <div class="footer-line">

      </div>

      <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" crossorigin="anonymous"></script>


      <!-- jQuery CDN - Slim version (=without AJAX) -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <!-- Popper.JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
      <!-- Bootstrap JS -->
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

      <script type="text/javascript">
        $(document).ready(function() {
          $('#sidebarCollapse').on('click', function() {
            $('#sidebar').toggleClass('active');
          });
        });
      </script>


</body>

</html>