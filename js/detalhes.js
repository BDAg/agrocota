// Cria a variavel e a assinatura do método XMLHttpRequest

var request = new XMLHttpRequest()

// Abre uma conecxão, faz a solicitação GET com a URL do endpoint

request.open('GET', 'https://api.github.com/user', true)

request.onload = function () {  
  var data = JSON.parse(this.response);
  var statusHTML = '';
  $.each(data, function(i, status) {
    statusHTML += '<tr>';
    statusHTML += '<td>' + status.id + '</td>';
    statusHTML += '<td>' + status.login + '</td>';
    statusHTML += '<td>' + status.html_url + '</td>';
    statusHTML += '<td>' + status.created_at + '</td>';
    statusHTML += '</tr>';
  });  
  $('tbody').html(statusHTML);
}

// encerra a solicitação

request.send();



