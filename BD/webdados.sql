-- MySQL Script generated by MySQL Workbench
-- Sat Sep 12 23:07:19 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema dougraf35_projetos
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dougraf35_projetos
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `desquinela_sqlserver` DEFAULT CHARACTER SET utf8 ;
USE `desquinela_sqlserver` ;

-- -----------------------------------------------------
-- Table `dougraf35_projetos`.`webdados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `desquinela_sqlserver`.`webdados` (
  `idCod` INT NOT NULL AUTO_INCREMENT,
  `vencimento` VARCHAR(10) NULL,
  `saca` VARCHAR(10) NULL,
  `variacao` VARCHAR(10) NULL,
  PRIMARY KEY (`idCod`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
